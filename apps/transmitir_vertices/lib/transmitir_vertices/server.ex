defmodule TransmitirVertices.Server do
  use GenServer

  ### Public API
  def start_link do
    GenServer.start_link(__MODULE__, [])
  end

  def recibir_vertices(pid) do
    GenServer.cast(pid, :recibir_vertices)
  end

  ### Server API
  def init() do
    {:ok, 0}
  end

  def handle_call(:recibir_vertices, state) do
    {:noreply, 1}
  end
end
