defmodule TransmitirVertices.ServerTest do
  use ExUnit.Case
  alias TransmitirVertices.Server

  test "starting the server" do
    assert {:ok, _pid} = Server.start_link()
  end

  test "transmitir vertices tupla" do
    assert {:ok, pid} = Server.start_link()
    :ok = Server.recibir_vertices(pid)
  end
end
