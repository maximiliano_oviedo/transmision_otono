# TransmitirVertices

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add transmitir_vertices to your list of dependencies in `mix.exs`:

        def deps do
          [{:transmitir_vertices, "~> 0.0.1"}]
        end

  2. Ensure transmitir_vertices is started before your application:

        def application do
          [applications: [:transmitir_vertices]]
        end

